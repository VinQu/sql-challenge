-- SQL Challenge Homework Data Analysis Queries

--Table of contents.
--1. Query 1
--2. Query 2
--3. Query 3
--4. Query 4
--5. Query 5
--6. Query 6
--7. Query 7
--8. Query 8

--1. List the following details of each employee: employee number, last name, first name, sex, and salary.
SELECT e.emp_no, e.last_name, e.first_name, e.sex, s.salary
	FROM employees AS e
	INNER JOIN salaries AS s 
	ON e.emp_no=s.emp_no;
	
--2. List first name, last name, and hire date for employees who were hired in 1986.
SELECT first_name, last_name, hire_date 
	FROM employees
	WHERE EXTRACT(YEAR FROM hire_date) = 1986
	
--3. List the manager of each department with the following information: department number, department name, the manager's employee number, last name, first name.
SELECT d.dept_no, d.dept_name, m.emp_no, e.last_name, e.first_name
	FROM departments as d
	INNER JOIN dept_manager AS m
	ON (d.dept_no=m.dept_no)
	JOIN employees as e
	ON (m.emp_no=e.emp_no)	

--4. List the department of each employee with the following information: employee number, last name, first name, and department name.
SELECT e.emp_no, e.last_name, e.first_name, d.dept_name
	FROM employees as e
	INNER JOIN dept_emp as de
	ON (e.emp_no=de.emp_no)
	JOIN departments as d
	ON (de.dept_no=d.dept_no)
	
--5. List first name, last name, and sex for employees whose first name is "Hercules" and last names begin with "B."
SELECT first_name, last_name, sex 
	FROM employees
	WHERE first_name = 'Hercules' AND SUBSTRING (last_name, 1, 1) = 'B'

--6. List all employees in the Sales department, including their employee number, last name, first name, and department name.
SELECT e.emp_no, e.last_name, e.first_name, d.dept_name
	FROM employees as e
	INNER JOIN dept_emp as de
	ON (e.emp_no=de.emp_no)
	JOIN departments as d
	ON (de.dept_no=d.dept_no)
	WHERE (d.dept_name='Sales')
	
--7. List all employees in the Sales and Development departments, including their employee number, last name, first name, and department name.
SELECT e.emp_no, e.last_name, e.first_name, d.dept_name
	FROM employees as e
	INNER JOIN dept_emp as de
	ON (e.emp_no=de.emp_no)
	JOIN departments as d
	ON (de.dept_no=d.dept_no)
	WHERE (d.dept_name='Sales') OR (d.dept_name='Development')
	
--8. In descending order, list the frequency count of employee last names, i.e., how many employees share each last name.
SELECT last_name, COUNT(last_name) as "total"
	FROM employees
	GROUP BY last_name
	ORDER BY total DESC