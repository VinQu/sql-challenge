-- SQL Challenge Homework Schema

--Table of contents.
--1. Creating tables and importing data
--2. Determining Relationships and adding PK or FK.

--1. Creating tables and importing data

-- Create new table
CREATE TABLE employees (
	emp_no INTEGER,
	emp_title VARCHAR,
	birth_date DATE,
	first_name VARCHAR,
	last_name VARCHAR,
	sex VARCHAR,
	hire_date DATE
);

-- View table and check imported data
SELECT * FROM employees;

-- Create new table
CREATE TABLE salaries (
	emp_no INTEGER,
	salary INTEGER
);

-- View table and check imported data
SELECT * FROM salaries;

-- Create new table
CREATE TABLE titles (
	title_id VARCHAR,
	title VARCHAR
);

-- View table and check imported data
SELECT * FROM titles;

-- Create new table
CREATE TABLE dept_emp (
	emp_no INTEGER,
	dept_no VARCHAR
);

-- View table and check imported data
SELECT * FROM dept_emp;

-- Create new table
CREATE TABLE departments (
	dept_no VARCHAR,
	dept_name VARCHAR
);

-- View table and check imported data
SELECT * FROM departments;

-- Create new table
CREATE TABLE dept_manager (
	dept_no VARCHAR,
	emp_no INTEGER
);

-- View table and check imported data
SELECT * FROM dept_manager;

--2. Determining Relationships and adding PK or FK.

-- Add Primary Keys.
ALTER TABLE employees
ADD CONSTRAINT PK_employees PRIMARY KEY (emp_no);

ALTER TABLE titles
ADD CONSTRAINT PK_titles PRIMARY KEY (title_id);

ALTER TABLE departments
ADD CONSTRAINT PK_departments PRIMARY KEY (dept_no);

-- Add Foreign Keys.
ALTER TABLE salaries
ADD CONSTRAINT FK_salaries_emp_no
FOREIGN KEY (emp_no) REFERENCES employees(emp_no);

ALTER TABLE employees
ADD CONSTRAINT FK_employees_title_id
FOREIGN KEY (emp_title) REFERENCES titles(title_id);

ALTER TABLE dept_emp
ADD CONSTRAINT FK_dept_emp_no
FOREIGN KEY (emp_no) REFERENCES employees(emp_no);

ALTER TABLE dept_emp
ADD CONSTRAINT FK_dept_emp_dept_no
FOREIGN KEY (dept_no) REFERENCES departments(dept_no);

ALTER TABLE dept_manager
ADD CONSTRAINT FK_dept_manager_emp_no
FOREIGN KEY (emp_no) REFERENCES employees(emp_no);

ALTER TABLE dept_manager
ADD CONSTRAINT FK_dept_manager_dept_no
FOREIGN KEY (dept_no) REFERENCES departments(dept_no);
